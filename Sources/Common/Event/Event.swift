//
//  Event.swift
//  Common
//
//  Created by Jon Sturk on 2019-04-16.
//

import Foundation

public protocol JSCEvent {
	associatedtype S //Source
	associatedtype T
	associatedtype OT
	
	var operation: JSCOperation<T> {get}
	var operationType: OT {get}
	
	var source: S? {get}
}

public enum JSCOperation<T> {
	case noop, unary(operand: T), binary(first: T, second: T)
}

public enum JSCArithmetic {
	case added, removed, changed, cleared
}

public struct JSCModelEvent<T>: JSCEvent {
	public init(source s: Any?, operation op: JSCOperation<T>, operationType ot: JSCArithmetic) {
		self.source = s
		self.operation = op
		self.operationType = ot
	}
	
	public var source: Any?
	public var operation: JSCOperation<T>
	public var operationType: JSCArithmetic
}
