import Foundation

//
//  Picker.swift
//  DummyGraph
//
//  Created by Jon Sturk on 31/07/14.
//  Copyright (c) 2014 Jon Sturk. All rights reserved.
//


public class Picker<T: Hashable>: JSCSynchronizedEventSource<JSCModelEvent<T>>, Equatable {
	typealias EventType = JSCModelEvent<T>
	let resourceName: String
	typealias HandlerFunc =  (_ evt:EventType) -> ()
	
	fileprivate var _picked: Set<T>
	
	public var picked: Set<T> {
		return _picked
	}
	
	public init(resourceName: String, picked p: Set<T> = Set<T>()) {
		self.resourceName = resourceName
		_picked = p
	}
	
	func togglePicked(_ obj: T) {
		if  _picked.contains(obj) {
			_picked.remove(obj)
			let evt = JSCModelEvent<T>(source: self, operation: .unary(operand: obj), operationType: .removed)
			notifyHandlers(evt)
		} else {
			_picked.insert(obj)
			let evt = JSCModelEvent<T>(source: self, operation: .unary(operand: obj), operationType: .added)
			notifyHandlers(evt)
		}
	}
	
	public func pick(_ obj: T, toggle: Bool = false) {
		if toggle {
			togglePicked(obj)
		} else {
			let contained = _picked.contains(obj)
			var tmp = _picked
			tmp.remove(obj)
			_picked = [obj]
			
			for rem in tmp {
				let evt = JSCModelEvent<T>(source: self, operation: .unary(operand: rem), operationType: .removed)
				notifyHandlers(evt)
			}
			
			if !contained {
				let evt = JSCModelEvent<T>(source: self, operation: .unary(operand: obj), operationType: .added)
				notifyHandlers(evt)
			}
		}
		let evt = JSCModelEvent<T>(source: self, operation: .noop, operationType: .changed)
		notifyHandlers(evt)
	}
	
	public func setPicked(_ obj: Set<T>, toggle: Bool) {
		if obj.count == 0 && !toggle {
			self.reset()
		}
		let removed = picked.subtracting(obj)
		for rem in removed {
			let evt = JSCModelEvent<T>(source: self, operation: .unary(operand: rem), operationType: .removed)
			notifyHandlers(evt)
		}
		let added = obj.subtracting(picked)
		for add in added {
			let evt = JSCModelEvent<T>(source: self, operation: .unary(operand: add), operationType: .added)
			notifyHandlers(evt)
		}
		
		let evt = JSCModelEvent<T>(source: self, operation: .noop, operationType: .changed)
		notifyHandlers(evt)
		
		_picked = obj
	}
	
	public func isPicked(_ obj: T) -> Bool {
		return _picked.contains(obj)
	}
	
	public func reset() {
		//let tmp = Array<T>(_picked)
		_picked.removeAll(keepingCapacity: false)
		//notifyListeners(.Removed, items: tmp)
		
		let evt = JSCModelEvent<T>(source: self, operation: .noop, operationType: .cleared)
		//let evt = WBModelEventAlt(operation: WBModelClearOperation<Array<T>>(operand: tmp), source: self)
		notifyHandlers(evt)
	}
	
	var count: Int {
		get {
			return _picked.count
		}
	}
}

public func == <T>(lhs: Picker<T>, rhs: Picker<T>) -> Bool {
	return lhs.resourceName == rhs.resourceName
}

enum PickType: String {
	case Added = "Added", Removed = "Removed", Cleared = "Cleared", Changed = "Changed"
}

