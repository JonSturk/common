//
//  EventSource.swift
//  Common
//
//  Created by Jon Sturk on 2019-04-16.
//

import Foundation

public protocol JSCEventSource: class {
	associatedtype E: JSCEvent
	
	func addHandler(_ handler: @escaping (E) -> Void) -> () -> Void
	func notifyHandlers(_ evt: E)
	//func removeAllHandlers() //<-- This causes the compiler to segfault (Xcode 10.2)
	
	var handlers: [Int: (E) -> Void] {get set}
	
	var muted: Bool {get set}
}

public protocol JSCDEventSourceImpl: JSCEventSource {
	
}

public extension JSCDEventSourceImpl {
	func addHandler(_ handler: @escaping (E) -> Void) -> () -> Void {
		let token = handlers.count
		handlers[token] = handler
		return {
			self.handlers[token] = nil
		}
	}
	
	func notifyHandlers(_ evt: E) {
		guard muted == false else {
			return
		}
		for (_, handler) in self.handlers {
			handler(evt)
		}
	}
	
	func removeAllHandlers() {
		handlers.removeAll(keepingCapacity: false)
	}
}


open class JSCDefaultEventSource<E: JSCEvent>: JSCDEventSourceImpl {
	public init() {}
	public typealias handlerFunc = (E) -> ()
	open var handlers = [Int: handlerFunc]()
	public var muted = false
}


open class JSCSynchronizedEventSource<E: JSCEvent>: JSCEventSource {
	private let queue: DispatchQueue
	
	public typealias handlerFunc = (E) -> ()
	public var handlers: [Int: handlerFunc] {
		get {
			return delegate.handlers
		}
		set {
			//XXX: We need this to conform to the protocol, but it should never be called.
		}
	}
	
	public var muted: Bool {
		get {
			return delegate.muted
		}
		set(val) {
			queue.async(flags: .barrier, execute: {
				self.delegate.muted = val
			})
		}
	}
	
	var delegate: JSCDefaultEventSource<E>
	
	public init() {
		delegate = JSCDefaultEventSource()
		queue = DispatchQueue(label: "EventSource worker-\(arc4random())", attributes: .concurrent)
	}
	
	public func addHandler(_ handler: @escaping (E) -> ()) -> () -> Void {
		var ret: (() -> Void)!
		queue.sync(flags: .barrier, execute: {
			let tmp = self.delegate.addHandler(handler)
			
			ret = {
				self.queue.async(flags: .barrier, execute: {
					tmp()
				})
			}
		})
		return ret
	}
	
	public func notifyHandlers(_ evt: E) {
		queue.async {
			self.delegate.notifyHandlers(evt)
		}
	}
	
	public func removeAllHandlers() {
		queue.async(flags: .barrier, execute: {
			self.delegate.removeAllHandlers()
		})
	}
}

